import { Router } from "express";
import { confirmVerification, login, logout, passwordReset, renewAccessToken, requestPasswordReset, sendVerificationEmail } from "../controllers/Auth.js";
import { prefetchUserByEmail } from "../controllers/User.js";
import { validate } from "../utils/handleValidations.js";
import { confirmPasswordResetSchema, confirmVerificationSchema, loginSchema, requestPasswordResetSchema, requestVerificationSchema } from "../validationSchemes/Auth.js";
import { verifyPasswordToken, verifyVerificationToken } from "../utils/handleTokens.js";

const router = Router();

/**
 * REQUEST VERIFICATION
 *
 * @param   {string}  email    [required] email address
 *
 * @return  {string}           re-sends a verification email & returns a related message
 */
router.post('/verification',
  validate(requestVerificationSchema),
  prefetchUserByEmail,
  sendVerificationEmail
);


/**
 * CONFIRM VERIFICATION
 *
 * @param   {string}  token   [required] verification token
 * @param   {string}  email   [required] email address
 *
 * @return  {string}          returns a related message
 */
router.patch('/verification',
  validate(confirmVerificationSchema),
  // prefetchUserByEmail,
  verifyVerificationToken,
  confirmVerification
);


/**
 * LOGIN
 * creates a new access token and refresh token
 * refresh token is delivered via cookie
 *
 * @param   {string}  email     [required] email address
 * @param   {string}  password  [required] password
 *
 * @return  {object}          user object with JWT
 */
router.post('/login',
  validate(loginSchema),
  login
);


/**
 * RENEW JWT
 * renews the short-living access token with the long-living refresh token
 * 
 * @param   {cookie}  renewAccessToken  [required] renewAccessToken via cookie
 *
 * @return  {object}                    JWT
 */
router.get('/',
  renewAccessToken
);

/**
 * LOGOUT
 * destroys the refresh token in the user record, so no new access token can be created
 *
 * @return  {object}           related message
 */
router.delete('/',
  logout
);


/**
 * REQUEST PASSWORD RESET
 *
 * @param   {email}  /password-reset  [required] email address
 *
 * @return  {object}           related message
 */
router.post('/password-reset',
  validate(requestPasswordResetSchema),
  requestPasswordReset
);


/**
 * PASSWORD RESET
 *
 * @param   {string}  token             [required] password reset token
 * @param   {string}  password          [required] password
 * @param   {string}  confirmPassword   [required] password
 * 
 * @return  {object}                   related message
 */
router.patch('/password-reset',
  validate(confirmPasswordResetSchema),
  verifyPasswordToken,
  passwordReset
);



export default router;