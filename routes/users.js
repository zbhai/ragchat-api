import { Router } from "express";
import { createUser, deleteUser, getUser, getUsers, prefetchUser, updateUser } from '../controllers/User.js';
import { gateKeeper, sendVerificationEmail } from '../controllers/Auth.js';
import { createUserSchema, updateUserSchema } from "../validationSchemes/User.js";
import { validate } from "../utils/handleValidations.js";
import { verifyAccessToken } from "../utils/handleTokens.js";


const router = Router();

/**
 * CREATE ONE
 *
 * @param   {string}  name            real name of the user [required]
 * @param   {string}  username        nickname [required]
 * @param   {string}  email           email address [required | email]
 * @param   {string}  password        password [required | strong ]
 * @param   {string}  confirmPassword password confirmation [required | must match password]
 *
 * @return  {string}           sends a verification email & returns a related message
 */
router.post('/', validate(createUserSchema), createUser, sendVerificationEmail);

/**
 * GET ONE
 * @header  {authorization}  Bearer       [required] access token
 * 
 * @prop    {string}          id          [required] id of the user to fetch
 * 
 */
router.get('/:id', verifyAccessToken, prefetchUser, getUser);

/**
 * GET MULTIPLE
 * @header  {authorization}  Bearer       [required] access token
 * 
 */
router.get('/', verifyAccessToken, gateKeeper, getUsers);


/**
 * UPDATE ONE
 * @header  {authorization}  Bearer       [required] access token
 * 
 * @prop    {string}          id          [required] id of the user to fetch  
 */
router.patch('/:id', verifyAccessToken, validate(updateUserSchema), prefetchUser, updateUser);


/**
 * DELETE ONE
 * @header  {authorization}  Bearer       [required] access token
 * 
 * @prop    {string}          id          [required] id of the user to delete
 */
router.delete('/:id', verifyAccessToken, deleteUser);

export default router;