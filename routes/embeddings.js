import { Router } from "express";
import { removeVectorDb, getStatus, updateEmbeddings } from "../controllers/Embeddings.js";
import { verifyAccessToken } from "../utils/handleTokens.js";
import { gateKeeper } from "../controllers/Auth.js";

const router = Router();

/**
 * REMOVE VECTOR DB
 * deletes the whole vector DB collection
 *
 * @header  {authorization}  Bearer       [required] access token
 *
 * @return  {object}                     related message
 */
router.delete('/', verifyAccessToken, gateKeeper, removeVectorDb);

/**
 * VECTOR DB STATUS
 * creates vector DB collection if not exists
 * returns the status of the vector DB
 *
 * @header  {authorization}  Bearer       [required] access token
 *
 * @return  {object}                     information about the vector DB collection
 */
router.get('/', verifyAccessToken, getStatus);

// update embeddings
/**
 * UPDATE EMBEDDINGS
 * removes orphaned and outdates embeddings
 * inserts updated and brand new embeddings
 *
 * @header  {authorization}  Bearer       [required] access token
 *
 * @return  {object}                     information about the update
 */
router.patch('/', verifyAccessToken, gateKeeper, updateEmbeddings);


export default router;