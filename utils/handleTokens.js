import jwt from 'jsonwebtoken';
import User from "../models/User.js";
import { findByIdAndUpdate, findOneAndUpdate, findOneRecord } from './handleDB.js';
import { performance } from "node:perf_hooks";


/**
 * generate a "oneTime" JWT, containing a given object
 * secret key & expire time come from .env
 * added user validation status to secret to invalidate this token after validating
 *
 * @param   {object}  payload  content of the token
 *
 * @return  {token}
 */
export const createVerificationToken = (payload) => {
  return jwt.sign({ id: payload.id, email: payload.email }, process.env.VERIFICATION_TOKEN_KEY, { expiresIn: process.env.VERIFICATION_TOKEN_TTL });
};

/**
 * verify that the Token exists and is untouched
 * expects req to contain the following
 * req.document to be a previously found user document
 * req.body.token to be the token to verify * 
 *
 * @param   req   request data
 * @param   res   response, sended to the user
 * @param   next  simply tell the code to go on with next function
 *
 */
export const verifyVerificationToken = async (req, res, next) => {
  // verify token
  const valid = jwt.verify(req.body.token, process.env.VERIFICATION_TOKEN_KEY, async (error, payload) => {
    // if invalid
    if (error) return res.status(498).json({ message: 'Token is no longer valid.' });
    // if valid
    req.body.email = payload.email;
    next();
  });
};


/**
 * generate a JWT, containing a given object
 * secret key & expire time come from .env
 *
 * @param   {object}  payload  content of the token
 *
 * @return  {token}
 */
export const createAccessToken = (payload) => {
  return jwt.sign(payload, process.env.JWT_SECRET_KEY, { expiresIn: process.env.JWT_TTL });
};

/**
 * generate a "oneTime" JWT, containing a given object
 * secret key & expire time come from .env
 * added user password to secret to invalidate this token after password change
 *
 * @param   {object}  payload  content of the token
 *
 * @return  {token}
 */
export const createPasswordToken = (payload) => {
  return jwt.sign({ id: payload.id, email: payload.email }, process.env.PASSWORD_TOKEN_KEY + payload.password, { expiresIn: process.env.PASSWORD_TOKEN_TTL });
};


/**
 * generate a JWT, containing a given object
 * secret key & expire time come from .env
 *
 * @param   {object}  payload  content of the token
 *
 * @return  {token}
 */
export const createRefreshToken = async (payload) => {
  try {
    // create
    const refreshToken = jwt.sign(payload, process.env.JWT_REFRESH_KEY);
    // save into user document
    await findByIdAndUpdate(User, payload.id, { refreshToken });
    // return
    return refreshToken;
  } catch (error) {
    throw error;
  }
};


/**
 * check if given refresh token exists in DB and is valid
 *
 * @param   {string}  refreshToken  refresh token
 *
 * @return  {any}         user from DB if exists, false if not
 */
export const verifyRefreshToken = async (refreshToken) => {
  try {
    // check if in DB
    const foundUser = await findOneRecord(User, { refreshToken });
    if (!foundUser) return false;
    // check if valid
    const user = jwt.verify(refreshToken, process.env.JWT_REFRESH_KEY);
    return (user && user.id === foundUser.id ? foundUser : false);
  } catch (error) {
    throw error;
  }
};

/**
 * delete given RefreshToken from user doc
 *
 * @param   {string}  refreshToken  hashed refresh token
 */
export const deleteRefreshToken = async (refreshToken) => {
  try {
    const user = await findOneAndUpdate(User, { refreshToken }, { $unset: { refreshToken: "" } });
    return;
  } catch (error) {
    throw error;
  }
};


/**
 * verify that the JWT exists and is untouched
 * If so, save user.id from token to res.currentUser for further computing
 *
 * @param   req   request data
 * @param   res   response, sended to the user
 * @param   next  simply tell the code to go on with next function
 *
 */
export const verifyAccessToken = async (req, res, next) => {
  performance.mark('verifyAccessToken:start');
  // define header
  const authHeader = req.headers['authorization'];
  // split token from authHeader - if available
  const token = authHeader && authHeader.split(' ')[1];

  // return if no token (or authHeader) found
  if (!token) return res.status(401).json({ message: 'No access token found. Access denied.' });

  // verify token
  performance.mark('jwt.verify:start');
  jwt.verify(token, process.env.JWT_SECRET_KEY, async (error, payload) => {
    performance.mark('jwt.verify:end');

    // if invalid
    if (error) return res.status(403).json({ message: 'Access token is no longer valid. Access denied.' });
    // if valid: remember current user id & role and go on
    global.currentUserId = payload.id;
    global.currentUserRole = payload.role;
    performance.mark('verifyAccessToken:end');

    next();
  });
};


/**
 * verify that the Token exists and is untouched
 * If so, save user.id from token to res.currentUser for further computing
 *
 * @param   req   request data
 * @param   res   response, sended to the user
 * @param   next  simply tell the code to go on with next function
 *
 */
export const verifyPasswordToken = async (req, res, next) => {
  try {
    // fetch user by token
    req.document = await findOneRecord(User, { resetPasswordToken: req.body.token }, '+password');  // verify token
    if (!req.document) return res.status(498).json({ message: 'Token is no longer valid.' });
    // check token validity
    jwt.verify(req.body.token, process.env.PASSWORD_TOKEN_KEY + req.document.password, async (error, payload) => {
      // if invalid
      if (error) return res.status(498).json({ message: 'Token is no longer valid.' });
      next();
    });
  } catch (error) {
    next(error);
  }
};