/**
 * Return extended error incl. statuscode, describing message and optional messages
 */
export class CustomError extends Error {
  constructor(error) {
    super(error.message);
    this.name = error.name;
    this.statusCode = error.statusCode || generateErrorStatusCode(error);
    this.body = generateErrorBody(error);
    this.isOperatinal = true;
    Error.captureStackTrace(this, this.constructor);
  }
}

/**
 * generate status codes by error names
 */
const generateErrorStatusCode = (error) => {
  switch (error.name) {
    // VALIDATION ERROR
    case "ValidationError":
    case "JsonWebTokenError":
    // ZOD VALIDATION ERROR
    case "zodError": {
      return 400;
    }
    // DEFAULT ERROR
    default: {
      return 500;
    }
  }
};

/**
 * create and return a individual error body
 */
const generateErrorBody = (error) => {
  switch (error.name) {
    // MONGOOSE VALIDATION ERROR
    case "ValidationError": {
      let validationErrors = {};
      Object.keys(error.errors).forEach((key) => {
        validationErrors[key] = error.errors[key].message;
      });
      // return error body
      return {
        message: 'Validation errors. Please check the error messages.',
        validationErrors
      };
    }
    // ZOD VALIDATION ERROR
    case "zodError": {
      let formattedErrors = error.format();
      // prepare object of all validation messages
      let validationErrors = {};
      // loop through all fields
      Object.keys(formattedErrors).forEach((key) => {
        // store field errors into messages
        const messages = formattedErrors[key]._errors;

        // if there are subkeys besides _errors
        if (Object.keys(formattedErrors[key]).length > 1) {
          // loop through subkeys 
          Object.keys(formattedErrors[key]).forEach((subkey) => {
            // skip _errors, as it's handled above
            if (subkey === "_errors") return;
            // skip undefined
            if (formattedErrors[key][subkey] == undefined) return;
            // get entry number which throws the error
            const entry = parseInt(subkey) + 1;
            // add subkey to messages
            messages.push(`Entry ${entry}: ${formattedErrors[key][subkey]._errors}`);
          });
        }
        // store messages in validationErrors
        if (messages !== undefined) validationErrors[key] = messages.join();
      });
      // return error body
      return {
        message: 'Validation errors. Please check the error messages.',
        validationErrors
      };
    }
    // DEFAULT ERROR
    default: {
      switch (error.statusCode) {
        case 405:
          // insufficient rights
          return { message: 'Operation not permitted. Insufficient access rights.' };
        default:
          // OTHER ERROR
          return { message: error.message };
      }
    }
  }
};

/**
 * used as middleware in router to fetch unknown routes
 */
export const middlewareUnknownRoute = (req, res, next) => {
  const error = new Error(`Route ${req.originalUrl} not found`);
  error.statusCode = 404;
  next(error);
};

/**
 * used as middleware in router to fetch and return errors
 */
export const middlewareErrorHandler = (error, req, res, next) => {

  // console.error("🚀 ~ middlewareErrorHandler ~ error:", error);
  console.error("🚀 ~ middlewareErrorHandler ~ error:", JSON.stringify(error));

  const customError = new CustomError(error);
  // return error to user
  res.status(customError.statusCode).json(customError.body);
};
