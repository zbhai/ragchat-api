import urlExist from "url-exist";
import { Ollama } from 'ollama';
import { ChatOllama } from "@langchain/community/chat_models/ollama";
import { ChatPromptTemplate, PromptTemplate, MessagesPlaceholder } from "@langchain/core/prompts";
import { embeddings, getRetriever, isCollectionAvailable, vectorStoreConnection } from "../controllers/Embeddings.js";
import { createHistoryAwareRetriever } from "langchain/chains/history_aware_retriever";
import { createStuffDocumentsChain } from "langchain/chains/combine_documents";
import { createRetrievalChain } from "langchain/chains/retrieval";
import { HumanMessage, AIMessage } from "@langchain/core/messages";
import { extendChat } from "./handleDB.js";
import * as path from 'path';
import { MultiQueryRetriever } from "langchain/retrievers/multi_query";
import {
  RunnableMap,
  RunnablePassthrough,
  RunnableSequence,
} from "@langchain/core/runnables";
import { formatDocumentsAsString } from "langchain/util/document";
import { StringOutputParser } from "@langchain/core/output_parsers";
import { ContextualCompressionRetriever } from "langchain/retrievers/contextual_compression";
import { LLMChainExtractor } from "langchain/retrievers/document_compressors/chain_extract";
import { RunnableBranch } from "@langchain/core/runnables";
import { pull } from "langchain/hub";
import { z } from "zod";
import { StructuredChatOutputParser } from "langchain/agents";
import { StructuredOutputParser } from "langchain/output_parsers";
import { EmbeddingsFilter } from "langchain/retrievers/document_compressors/embeddings_filter";

// PROVIDE OLLAMA CONNECTION TO ALL ROUTES
export const ollama = new Ollama({ host: process.env.AI_API_URL });

/** *******************************************************
 * CHECK IF OLLAMA API IS AVAILABLE
 */
export const aiIsRunning = async () => {
  try {
    return await urlExist(`${process.env.AI_API_URL}`);
  } catch (error) {
    return false;
  }
};

/** *******************************************************
 * FILTER INSTALLED MODELS BY NAME VIA REGEX
 */
export const aiGetModels = async () => {
  try {
    return await ollama.list();
  } catch (error) {
    throw error;
  }
};


/** *******************************************************
 * GET MODEL
 */
export const aiGetModel = async (model) => {
  try {
    return await ollama.show({ model });
  } catch (error) {
    throw error;
  }
};

/** *******************************************************
 * INSTALL MODEL
 */
export const aiInstallModel = async (model) => {
  try {
    const message = await ollama.pull({ model, stream: false });
    return { message: `model ${model} installed` };
  } catch (error) {
    throw error;
  }
};


/** *******************************************************
 * DELETE MODEL
 */
export const aiDeleteModel = async (model) => {
  try {
    const message = await ollama.delete({ model });
    return { message: `model ${model} deleted` };
  } catch (error) {
    throw error;
  }
};


/** *******************************************************
 * CREATE AI SUMMARIZED TEXT
 */
export const summarizeText = async (model, input) => {
  try {
    performance.mark('summarizeText:start');
    // define llm
    const llm = new ChatOllama({
      baseUrl: process.env['AI_API_URL'],
      model: model,
      temperature: Number(process.env['AI_TEMPERATURE'])
    });
    // create template
    const promptTemplate = PromptTemplate.fromTemplate(process.env['AI_SUMMARIZE_PROMPT']);
    // create chain combining llm and template
    const chain = promptTemplate.pipe(llm);
    // invoke variable text & run chain
    const summary = await chain.invoke({ text: input });
    performance.mark('summarizeText:end');
    return summary.content;
  } catch (error) {
    throw error;
  }
};

/** *******************************************************
 * GENERATE CHAT
 */
export const chat = async (req, res, next) => {
  performance.mark('chat:start');
  // #################
  // test if collection is available
  // #################
  const collection = await isCollectionAvailable();
  if (!collection) {
    return res.status(500).json({ error: `VectorDB collection ${process.env['VECTOR_COLLECTION_NAME']} not found.` });
  }

  // #################
  // init chat model
  // #################
  performance.mark('chat-InitModel:start');
  const model = new ChatOllama({
    baseUrl: process.env['AI_API_URL'],
    model: req.body.model,
    temperature: Number(process.env['AI_TEMPERATURE'])
  });
  performance.mark('chat-InitModel:end');

  // #################
  // init retriever
  // #################
  performance.mark('chat-InitCompressionRetriever:start');
  let vectorStore = await vectorStoreConnection();
  // const baseCompressor = LLMChainExtractor.fromLLM(model);
  const baseCompressor = new EmbeddingsFilter({
    embeddings: embeddings,
    similarityThreshold: 0.8,
  });
  const retriever = new ContextualCompressionRetriever({
    baseCompressor,
    baseRetriever: vectorStore.asRetriever(),
  });
  performance.mark('chat-InitCompressionRetriever:end');

  // #################
  // prepare contextualize question
  // {question}[String] + {chat_history}[MessagesArray] = {question}[String]
  // #################
  performance.mark('chat-prepareContextualization:start');
  // define a prompt
  const contextualizeQPrompt = ChatPromptTemplate.fromMessages([
    ["system", process.env.AI_CONTEXTUALIZE_HISTORY_PROMPT],
    new MessagesPlaceholder("chat_history"),
    ["human", "{input}"],
  ]);
  // define a chain
  const contextualizeQChain = contextualizeQPrompt
    .pipe(model)
    .pipe(new StringOutputParser());
  performance.mark('chat-prepareContextualization:end');

  // #################
  // Q & A
  // {question}[String] + {context}[string] = {answer}[String]
  // #################
  performance.mark('chat-runMainChain:start');
  // genereate a prompt
  const qaPrompt = ChatPromptTemplate.fromMessages([
    ["system", process.env.AI_QA_PROMPT],
    new MessagesPlaceholder("chat_history"),
    ["human", "{question}"],
  ]);

  // define a chain
  const mainChain = RunnablePassthrough.assign({
    question: (invokeInput) => {
      // if history given 
      if (invokeInput.chat_history.length > 0) {
        // input into contextualized question
        return contextualizeQChain;
      }
      // simply return input if no history given
      return invokeInput.input;
    }
  }).assign({
    docs: async (invokeInput) => {
      // fetch question related docs from retriever
      return await retriever.invoke(invokeInput.question);
    }
  }).assign({
    context: async (invokeInput) => {
      // serialize docs to make them a context string
      return formatDocumentsAsString(invokeInput.docs);
    }
  }).assign({
    answer: async (invokeInput) => {
      // try to fetch answer, based on context
      return qaPrompt.pipe(model).pipe(new StringOutputParser());
    }
  });


  // invoke question + chat_history = contextualized question + answer + docs
  const results = await mainChain.invoke({
    input: req.body.input,
    chat_history: req.body.chatHistory ?? []
  });
  performance.mark('chat-runMainChain:end');

  // store q/a-pair in chat history
  performance.mark('chat-saveHistory:start');
  let chat = await extendChat(req.body.chatId, [
    new HumanMessage(req.body.input),
    new AIMessage({ content: results.answer, source: results.docs[0] })
  ]);
  performance.mark('chat-saveHistory:end');

  performance.mark('chat:end');
  return res.json({ results, chat });
};
