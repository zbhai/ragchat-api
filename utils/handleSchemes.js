
/**
 * get confidential fields from given schema based on attribute select: false
 *
 * @param   {model}  model  model to compute
 *
 * @return  {array}         matching field names 
 */
export const getConfidentialFields = (model) => {
  performance.mark('getConfidentialFields:start');
  const schema = Object.entries(model.schema.paths);
  const confidentialFields = schema.filter(function (field) {
    return field[1].selected === false;
  });
  performance.mark('getConfidentialFields:end');
  return confidentialFields.map(field => field[0]);
};


/**
 * remove fields from given object that are stated as confidential
 *
 * @param   {model}  model   model to compute
 * @param   {object}  object  object to compute
 *
 * @return  {object}          cleansed object
 */
export const hideConfidentialFields = (model, record) => {
  performance.mark('hideConfidentialFields:start');
  // turn mongoose record into js object
  // const object = record.toObject();
  const object = record;
  // get confidential fields from model
  const confidentialFields = getConfidentialFields(model);
  // delete confidential fields from object
  confidentialFields.forEach(field => delete object[field]);
  performance.mark('hideConfidentialFields:end');
  return object;
};



/**
 * get array of all field names from a given model
 *
 * @param   {model}  model  model to compute
 *
 * @return  {array}         all found field names
 */
const getAllFieldnames = (model) => {
  return Object.keys(model.schema.paths);
};

/**
 * get an object with key:value pairs
 * wipe out all fields, not corresponding with given model
 *
 * @param   {model}  model  model to compute
 * @param   {object}  object  object, contains key:value
 *
 * @return  {object}          object with key:values only for fields, found in schema
 */
export const prefillDocumentObject = (model, object) => {
  const allowedFields = getAllFieldnames(model);
  let result = Object.fromEntries(allowedFields.map((field) => {
    if (Object.hasOwnProperty.bind(object)(field)) {
      return [field, object[field]];
    }
  }).filter(field => field));
  return result;
};
