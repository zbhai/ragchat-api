import { CustomError } from "./handleErrors.js";

// ### Validation function
export const validate = (schema) => (req, res, next) => {
  performance.mark('validateInput:start');
  try {
    // validate provided schema against request body
    schema.parse(req.body);
    // advance
    performance.mark('validateInput:end');
    next();
  } catch (error) {
    // name error
    error.name = 'zodError';
    error.statusCode = 400;
    // send error to global error handler
    next(error);
  }
};
