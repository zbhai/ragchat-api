import chalk from 'chalk';
import nodemailer from 'nodemailer';

export const sendEmail = async options => {
  console.log('original sendEmail');
  // create reusable transporter object using the default SMTP transport
  const transporter = nodemailer.createTransport({
    host: process.env.SMTP_HOST,
    port: process.env.SMTP_PORT,
    auth: {
      user: process.env.SMTP_USER,
      pass: process.env.SMTP_PASSWORD
    }
  });

  // send mail with defined transport object
  // TODO email templates nutzen
  const message = {
    from: `${process.env.SMTP_FROM_NAME} <${process.env.SMTP_FROM_ADDRESS}>`, // sender address
    to: options.to, // list of receivers
    subject: options.subject, // Subject line
    text: options.text, // plain text body
    html: options.html // plain text body
  };

  const info = await transporter.sendMail(message);

  console.log(chalk.green('Message sent: %s', info.messageId));
};