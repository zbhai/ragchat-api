import express from 'express';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import { createRecord, dbConnection, findOneRecord } from './utils/handleDB.js';
import { middlewareErrorHandler, middlewareUnknownRoute } from './utils/handleErrors.js';

/**
 * start a performance observer
 * watches for performance.mark entries
 * 
 * each entry must follow the naming convention:
 * performance.mark('<arbitaryName>:start');
 * performance.mark('<arbitaryName>:end');
 * 
 */
const observer = new PerformanceObserver((list, observer) => {
  for (const entry of list.getEntries()) {
    try {
      // check if mark is an endpoint
      if (entry.name.endsWith(':end')) {
        // define names, based on entry name
        const measurementName = entry.name.replace(':end', '');
        const startName = `${measurementName}:start`;
        // calculate
        const measurement = performance.measure(measurementName, startName, entry.name);
        // log
        if (process.env.DEBUG_PERFORMANCE === 'true') {
          console.info({ name: measurement.name, duration: `${measurement.duration} ms` });
        }

        // clear
        performance.clearMarks(entry.name);
        performance.clearMarks(startName);
      }
    } catch (error) {
      // log
      console.error(error.message);
      // clear
      performance.clearMarks(entry.name);
    }
    // observer.disconnect();
  }
});
observer.observe({ entryTypes: ['mark'] });

/**
 * establish DB connection
 */
const db = dbConnection();

// create superadmin on first run
const isSuperAdminAvailable = await findOneRecord(User, { email: process.env.SUPERADMIN_EMAIL });

if (!isSuperAdminAvailable) {
  const newSuperAdmin = await createRecord(User, {
    name: 'RAGChat Admin',
    username: 'superAdmin',
    email: process.env.SUPERADMIN_EMAIL,
    password: process.env.SUPERADMIN_PASSWORD,
    role: 4,
    verified: true
  });
  console.log(chalk.green('created superAdmin'));
}

/**
 * configure app
 */
// create the app, which is an express instance
const app = express();
app.use(express.static('uploads'));

// define where to run the app at
// allow app to accept json as a body inside of requests
app.use(express.json());
// enable use cookie-parser
app.use(cookieParser());

// define frontUrl
global.frontURL = process.env.FRONTEND_URL.split(',')[0];
// turn origins into array
const origins = process.env.FRONTEND_URL.split(',');
// turn array items into regex
for (let i = 0; i < origins.length; i++) {
  origins[i] = new RegExp(origins[i]);
}
// combine all cors options into object
const corsOptions = {
  origin: origins,
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 20
  credentials: true,
};
// make use of cors options
app.use(cors(corsOptions));


/**
 * import routes
*/
import usersRouter from './routes/users.js';
import authRouter from './routes/auth.js';
import aiRouter from './routes/ai.js';
import embeddingsRouter from './routes/embeddings.js';
import User from './models/User.js';
import chalk from 'chalk';


/**
 * use routes
*/
app.use('/users', usersRouter);
app.use('/auth', authRouter);
app.use('/ai', aiRouter);
app.use('/embeddings', embeddingsRouter);
// 404
app.all('*', middlewareUnknownRoute);
// global error middleware
app.use(middlewareErrorHandler);

export default app;