import { z } from 'zod';

// GET MODELS
export const getModelsSchema = z.object({
  filter: z.string(),
});

// GET MODEL
export const getModelSchema = z.object({
  model: z.string().min(1),
});

// INSTALL MODEL
export const installModelSchema = z.object({
  model: z.string().min(1),
  stream: z.boolean().optional(),
});

// DELETE MODEL
export const deleteModelSchema = z.object({
  model: z.string().min(1),
});

// CHAT
export const chatSchema = z.object({
  model: z.string().min(1),
  input: z.string().min(1),
  chatId: z.string().min(1).optional(),
});