import { z } from 'zod';
import validator from 'validator';

// SIGN UP
export const createUserSchema = z.object({
  name: z.string().min(1),
  username: z.string().min(1),
  email: z.string().email(),
  password: z.string().refine((val) => val && validator.isStrongPassword(val), {
    message: 'This value must be min 6 characters long and contain uppercase, lowercase, number, specialchar.',
  }),
  confirmPassword: z.string(),
}).refine((data) => data.password === data.confirmPassword, {
  message: "Passwords don't match",
  path: ["confirmPassword"],
});

// UPDATE
export const updateUserSchema = z.object({
  name: z.string().min(1),
  username: z.string().min(1),
  email: z.string().email(),
  verified: z.boolean(),
  password: z.string().refine((val) => val && isStrongPassword(val), {
    message: 'This field must be min 6 characters long and contain uppercase, lowercase, number, specialchar.',
  }).nullish().or(z.literal('')),
  confirmPassword: z.string().nullish().or(z.literal('')),
}).refine((data) => data.password === data.confirmPassword, {
  message: "Passwords don't match",
  path: ["confirmPassword"],
});
