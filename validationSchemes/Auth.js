import { z } from 'zod';
import validator from 'validator';

// REQUEST VERIFICATION
export const requestVerificationSchema = z.object({
  email: z.string().email(),
});

// CONFIRM VERIFICATION
export const confirmVerificationSchema = z.object({
  token: z.string().min(1),
});

// LOGIN
export const loginSchema = z.object({
  email: z.string().min(1).email(),
  password: z.string().min(1)
});

// REQUEST PASSWORD RESET
export const requestPasswordResetSchema = z.object({
  email: z.string().min(1).email(),
});

// CONFIRM PASSWORD RESET
export const confirmPasswordResetSchema = z.object({
  token: z.string().min(1),
  password: z.string().refine((val) => val && validator.isStrongPassword(val), {
    message: 'This value must be min 6 characters long and contain uppercase, lowercase, number, specialchar.',
  }),
  confirmPassword: z.string(),
}).refine((data) => data.password === data.confirmPassword, {
  message: "Passwords don't match",
  path: ["confirmPassword"],
});