import { Schema, model } from 'mongoose';
import mongooseUniqueValidator from 'mongoose-unique-validator';

// ################################# SCHEMA OPTIONS
const opts = {
  timestamps: true,
  toJSON: { virtuals: true },
  strict: true,
  strictQuery: false // Turn off strict mode for query filters
};

// ################################# SCHEMA
const ChatSchema = new Schema(
  {
    title: {
      type: String,
      title: 'Title',
      required: true
    },
    chatHistory: {
      type: Object,
      title: 'Description',
    },
    updatedBy: {
      type: Schema.Types.ObjectId,
      title: 'updated by',
      ref: "User"
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      title: 'created by',
      ref: "User"
    }
  },
  opts
);

// ################################# VIRTUALS


// ################################# MIDDLEWARES
// middleware pre|post on validate|save|remove|updateOne|deleteOne 
// pre save
ChatSchema.pre('save', async function (next) {
  // remember editor
  if (this.isNew) {
    this.createdBy = global.currentUserId;
  } else {
    this.updatedBy = global.currentUserId;
  }
  // go on
  next();
});



// ################################# STATICS

// ################################# QUERY HELPERS
ChatSchema.pre('find', function (next) {
  // set default sort
  if (typeof this.options.sort === 'undefined') {
    this.options.sort = { createdAt: -1 };
  }
  next();
});

// ################################# INSTANCE METHODS
/**
 * Validate unique fields in the following function
 * because default mongoose 'unique: true' isn't handled as validation error
 */
ChatSchema.plugin(mongooseUniqueValidator, { message: 'Record with this {PATH} already exists.' });

/**
 * return all confidential fieldnames
 * based on 'select: false'
 */
ChatSchema.methods.getConfidentialFields = function () {
  const schema = Object.entries(Chat.schema.paths);
  const confidentialFields = schema.filter(function (field) {
    return field[1].selected === false;
  });
  return confidentialFields.map(field => field[0]);
};

export default model('Chat', ChatSchema);