import { Schema, model } from 'mongoose';
import bcrypt from 'bcrypt';
import mongooseUniqueValidator from 'mongoose-unique-validator';


// ################################# SCHEMA OPTIONS
const opts = {
  timestamps: true,
  toObject: { virtuals: true },
  toJSON: { virtuals: true },
  strict: true,
  strictQuery: false // Turn off strict mode for query filters
};

// ################################# SCHEMA
const UserSchema = new Schema(
  {
    username: {
      type: String,
      title: 'Username'
    },
    name: {
      type: String,
      title: 'Name'
    },
    email: {
      type: String,
      title: 'eMail',
      required: true,
      lowercase: true,
      unique: true,
    },
    password: {
      type: String,
      title: 'Password',
      select: false
    },
    resetPasswordToken: {
      type: String,
      title: 'Password Token'
    },
    verified: {
      type: Boolean,
      title: 'Validated',
      default: false
    },
    role: {
      type: Number,
      title: 'Role',
      default: 0
    },
    refreshToken: {
      type: String,
      title: 'Refresh Token',
      select: false
    },
    updatedBy: {
      type: Schema.Types.ObjectId,
      title: 'updated by',
      ref: "User"
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      title: 'created by',
      ref: "User"
    }
  },
  opts
);

// ################################# VIRTUALS
// fullName
// UserSchema.virtual('fullname').get(function () {
//   return `${this.title || ''} ${this.firstname || ''} ${this.lastname || ''}`.replace(/\s+/g, ' ').trim();
// });

// ################################# MIDDLEWARES
// middleware pre|post on validate|save|remove|updateOne|deleteOne 
// pre save
UserSchema.pre('save', async function (next) {
  // remember editor
  if (this.isNew) {
    this.createdBy = global.currentUserId;
  } else {
    this.updatedBy = global.currentUserId;
  }

  // hash password
  if (this.isModified('password')) {
    const hashedPassword = await bcrypt.hash(this.password, Number(process.env.BCRYPT_STRENGTH));
    this.password = hashedPassword;
  }
  // go on
  next();
});



// ################################# STATICS
// example static function | only avail on User directly like User.findByMail('a@b.c')
// UserSchema.statics.findByMail = function(email){
//   return this.find({email: new RegExp(email, "i")})
// }

// ################################# QUERY HELPERS
// example chainable query function | only avail chained on User query like User.find().byMail('a@b.c')
// UserSchema.query.byMail = function(email){
//   return this.where({email: new RegExp(email, "i")})
// }
UserSchema.pre('find', function (next) {
  // set default sort
  if (typeof this.options.sort === 'undefined') {
    this.options.sort = { username: 1 };
  }
  next();
});

// ################################# INSTANCE METHODS
// example method
// UserSchema.methods.fullName = function () {
//   return `${this.title} ${this.firstname} ${this.lastname}`;
// };


/**
 * Validate unique fields in the following function
 * because default mongoose 'unique: true' isn't handled as validation error
 */
UserSchema.plugin(mongooseUniqueValidator, { message: 'Record with this {PATH} already exists.' });

/**
 * return all confidential fieldnames
 * based on 'select: false'
 */
UserSchema.methods.getConfidentialFields = function () {
  const schema = Object.entries(User.schema.paths);
  const confidentialFields = schema.filter(function (field) {
    return field[1].selected === false;
  });
  return confidentialFields.map(field => field[0]);
};

export default model('User', UserSchema);