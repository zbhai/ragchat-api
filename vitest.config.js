import { configDefaults, defineConfig } from 'vitest/config';

export default defineConfig({
  test: {
    include: ['__tests__/**/*.test.js'],
    // clearMocks: true,
    // mockReset: true,
    mockRestore: true,
    coverage: {
      exclude: [
        './ecosystem.config.cjs',
        './server.js',
        './vitest.config.js',
        '**/controllers/Embeddings.js',
        '**/utils/handleAI.js',
        '**/utils/handleDB.js',
        '**/utils/handleMailer.js',
      ]
    }
  },
});