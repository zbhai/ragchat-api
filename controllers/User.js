import User from "../models/User.js";
import { createRecord, deleteOneRecord, findByIdAndUpdate, findOneRecord, findRecordByID, findRecords } from "../utils/handleDB.js";
import { prefillDocumentObject, hideConfidentialFields } from '../utils/handleSchemes.js';

/** *******************************************************
 * CREATE ONE
 */
export const createUser = async (req, res, next) => {
  try {
    // autoverify if user-agent is Artillery
    const isArtilleryAgent = (req.get('user-agent') && req.get('user-agent').includes('Artillery') ? true : false);
    if (isArtilleryAgent) {
      req.body.verified = true;
    }
    // create user object
    const newRecord = await createRecord(User, prefillDocumentObject(User, req.body));
    // remember document but remove confidential info
    req.document = hideConfidentialFields(User, newRecord);
    // return if user-agent is Artillery
    if (isArtilleryAgent) {
      return res.status(201).json({ message: 'User created', document: req.document });
    }
    // continue to send verification
    next();
    // on error
  } catch (error) {
    next(error);
  };
};


/** *******************************************************
 * GET ONE
 */
export const getUser = (req, res, next) => {
  return res.json(req.requestedDocument);
};

/** *******************************************************
 * GET MULTIPLE
 */
export const getUsers = async (req, res, next) => {
  try {
    const users = await findRecords(User, {});
    return res.json(users);
  } catch (error) {
    next(error);
  }
};


/** *******************************************************
 * UPDATE ONE
 */
export const updateUser = async (req, res, next) => {
  // check if user is allowed to change data
  // if not self editing 
  if (global.currentUserId !== req.requestedDocument.id) {
    // check for current users role
    if (!(global.currentUserRole >= 2)) return res.status(403).json({ message: 'Access forbidden' });
  }

  // filter req data by schema field names
  const newData = prefillDocumentObject(User, req.body);
  // drop password if empty to prevent setting empty
  if (!newData.password) delete newData.password;
  // merge new data into document
  Object.assign(req.requestedDocument, newData);
  // try saving
  try {
    // const updatedUser = await req.requestedDocument.save({ new: true });
    const updatedUser = await findByIdAndUpdate(User, req.requestedDocument.id, req.requestedDocument);
    const document = hideConfidentialFields(User, updatedUser);
    // return msg incl. document
    res.json({ message: 'User successfully updated', document });
  } catch (error) {
    next(error);
  }
};

/** *******************************************************
 * DELETE ONE
 */
export const deleteUser = async (req, res, next) => {
  // check if user is allowed to change data
  // if not self editing 
  if (global.currentUserId !== req.params.id) {
    // check for current users role
    if (!(global.currentUserRole >= 2)) return res.status(403).json({ message: 'Access forbidden' });
  }

  try {
    // delete document
    console.log("🚀 ~ deleteUser ~ req.params.id:", req.params.id);
    const document = await deleteOneRecord(User, req.params.id);
    // return msg incl. document
    return res.status(200).json({ message: 'User successfully deleted', document });
  } catch (error) {
    console.error(error);
    next(error);
  }
};


/** *******************************************************
 ####################### FUNCTIONS #######################
 ******************************************************* */

/** *******************************************************
 * FIND USER BY MAIL
 */
export const prefetchUserByEmail = async (req, res, next) => {
  try {
    // search for matching document
    const foundUser = await findOneRecord(User, { email: req.body.email });
    // if user not found
    if (!foundUser) {
      // send message
      return res.status(404).json({ message: 'Unknown eMail address' });
    }
    // if user found, store in req for further computing
    req.document = foundUser;
    next();
  } catch (error) {
    next(error);
  }
};


/**
 * get a document based on ID from request param 
 * save this to req.requestedDocument for further computing
 *
 * @param   req   request data
 * @param   res   response, sended to the user
 * @param   next  simply tell the code to go on with next function
 *
 */
export const prefetchUser = async (req, res, next) => {

  try {
    if (!req.params.id) return res.status(400).json({ message: 'No ID given' });
    // search for matching document
    // FIX Mongoose .populate() für createdBy & updatedBy (updatedBy wird bei Update überschrieben und ist nicht länger populated)
    const user = await findRecordByID(User, req.params.id);
    // if no matching document was found
    if (!user._id) return res.status(404).json({ message: 'Cannot find user' });
    // remember document in res
    req.requestedDocument = user;
    // going on with the rest of the code
    next();
  } catch (error) {
    // on error
    next(error);
  }
};