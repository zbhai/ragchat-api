import app from './app.js';
import https from 'https';
import fs from 'fs';

/**
 * prepare httpS
 */
let proto = 'http';
let server = app;
// get SSL key & cert
const key = fs.readFileSync(process.env.API_SSL_KEY);
const cert = fs.readFileSync(process.env.API_SSL_CERT);

console.log('API_MODE:', process.env.API_MODE);

if (process.env.API_MODE === 'devel') {
  server = https.createServer({ key: key, cert: cert }, app);
  proto = 'https';
}
const BASE_URL = `${proto}://${process.env.API_HOST}:${process.env.API_PORT}`;

/**
 * finally start the server and run callback funtion on success
 */
server.listen(process.env.API_PORT, process.env.API_HOST, () => {
  console.log(`API LISTENING ON: ${BASE_URL}`);
  console.log('To exit CTRL + C');
});