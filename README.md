# Purposes
This repository provides the connection to a ollama server.
It's an API (suprise!), which is bound to an inbuild PocketBase authentification system.

The main benefit of using this repo is, that it's running 100% on your machines. No data ever leaves your network. 

# Prerequisits
- [Ollama](https://ollama.com/download) running & reachable
- [ChromaDB](https://www.trychroma.com/) running & reachable
- [MongoDB](https://www.mongodb.com/) running & reachable
- [npm](https://www.npmjs.com/) installed

# Install
```
git clone git@gitlab.rrz.uni-hamburg.de:zbhai/ragchat-api.git
cd ragchat-api
npm i
```

# Configure
populate the `.env` file with proper information

# Start
```
npm start
```
optional - if [pm2](https://pm2.keymetrics.io/) installed
```
pm2 start ./ecosystem.config.cjs
```

# Sources

## Ollama
- [Ollama API](https://github.com/ollama/ollama/blob/main/docs/api.md)
- [Ollama js](https://github.com/ollama/ollama-js)

## PocketBase
- [PocketBase Docs](https://pocketbase.io/docs/)
- [PocketBase JS SDK](https://github.com/pocketbase/js-sdk)

## ChromaDB
- [Chroma](https://www.trychroma.com/)
- [CheatSheet](https://gist.github.com/danielduckworth/b318e1297d9d953a2fe127738ce9e67c)

# Roadmap
- [ ] polishing