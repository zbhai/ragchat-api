// import vitest, supertest & app
import { vi, beforeAll, beforeEach, describe, expect, expectTypeOf, test, it, afterEach } from 'vitest';
import supertest from "supertest";
import app from "../../app.js";
import jwt from 'jsonwebtoken';

// set route
const ROUTE = '/users/66a29da2942b3ebcaf047f07';
// prepare response of each test
let response;

// ############################
//  OBJECTS
// ############################
const mockedVals = vi.hoisted(() => {
  return {
    foundUser: {
      _id: '66a29da2942b3eb',
      username: 'snoopy',
      name: 'My User',
      email: 'user@mail.local',
      verified: true,
      role: 0,
      createdAt: '2024-07 - 25T18: 46: 58.982Z',
      updatedAt: '2024-07 - 25T18: 46: 58.982Z',
      __v: 0,
      fullname: '',
      id: '66a29da2942b3ebcaf047f07'
    }
  };
});

// ############################
//  MOCKS
// ############################
// import Database Service
import * as dbService from '../../utils/handleDB.js';
// mock dbService
vi.mock('../../utils/handleDB.js', async (importOriginal) => {
  return {
    ...await importOriginal(),
    dbConnection: vi.fn(() => 'mocked'),
    findOneRecord: vi.fn(() => mockedVals.foundUser),
    findRecordByID: vi.fn(() => mockedVals.foundUser)
  };
});

// ############################
//  TESTS
// ############################
describe('user find by ID', () => {
  const _jwt = (id, role) => {
    return jwt.sign({ id, role }, process.env.JWT_SECRET_KEY, { expiresIn: process.env.JWT_TTL });
  };

  describe('given the inputs are valid', async () => {
    // set response by running route
    beforeAll(async () => {
      response = await supertest(app)
        .get(ROUTE)
        .set('Authorization', `Bearer ${_jwt(mockedVals.foundUser.id, mockedVals.foundUser.role)}`)
        .send();
    });

    it('should return a proper status code', () => {
      expect(response.status).toBe(200);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given no valid jwt sended', () => {
    beforeAll(async () => {
      response = await supertest(app)
        .get(ROUTE)
        .set('Authorization', `Bearer invalid`)
        .send();
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(403);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given no jwt sended', () => {
    beforeAll(async () => {
      response = await supertest(app)
        .get(ROUTE)
        .send();
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(401);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });
});