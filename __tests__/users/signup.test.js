// import vitest, supertest & app
import { vi, beforeAll, beforeEach, describe, expect, expectTypeOf, test, it, afterEach } from 'vitest';
import supertest from "supertest";
import app from "../../app.js";

// set route
const ROUTE = '/users';
// prepare response of each test
let response;

// ############################
//  OBJECTS
// ############################
const mockedVals = vi.hoisted(() => {
  return {
    foundUser: {
      _doc: {
        _id: '66a29da2942b3eb',
        username: 'snoopy',
        name: 'My User',
        email: 'user@mail.local',
        verified: true,
        role: 0,
        createdAt: '2024-07 - 25T18: 46: 58.982Z',
        updatedAt: '2024-07 - 25T18: 46: 58.982Z',
        __v: 0,
        fullname: '',
        id: '66a29da2942b3ebcaf047f07'
      }
    },
    validInput: {
      name: 'My User',
      username: 'snoopy',
      email: 'user@mail.local',
      password: 'StrongPass1!',
      confirmPassword: 'StrongPass1!'
    }
  };
});

// ############################
//  MOCKS
// ############################
// import Database Service
import * as dbService from '../../utils/handleDB.js';
// mock dbService
vi.mock('../../utils/handleDB.js', async (importOriginal) => {
  return {
    ...await importOriginal(),
    dbConnection: vi.fn(() => 'mocked'),
    findOneRecord: vi.fn(() => mockedVals.foundUser),
    createRecord: vi.fn(() => mockedVals.foundUser)
  };
});
// mock mailer
vi.mock('../../utils/handleMailer.js', async (importOriginal) => {
  return {
    ...await importOriginal(),
    sendEmail: vi.fn(() => 'mocked')
  };
});

// ############################
//  TESTS
// ############################
describe('user registration', () => {
  describe('given the inputs are valid', async () => {
    // set response by running route
    beforeAll(async () => {
      response = await supertest(app)
        .post(ROUTE)
        .send(mockedVals.validInput);
    });

    it('should return a proper status code', () => {
      expect(response.status).toBe(201);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('the request body is empty', () => {
    beforeAll(async () => {
      response = await supertest(app)
        .post(ROUTE)
        .send();
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(400);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given the password and confirmPassword do not match', () => {
    beforeAll(async () => {
      const input = { ...mockedVals.validInput, confirmPassword: 'StrongPass2!' };

      response = await supertest(app)
        .post(ROUTE)
        .send(input);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(400);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given required fields are missing', () => {
    beforeAll(async () => {
      const { email, ...input } = mockedVals.validInput;

      response = await supertest(app)
        .post(ROUTE)
        .send(input);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(400);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given the email format is invalid', () => {
    beforeAll(async () => {
      const input = { ...mockedVals.validInput, email: 'invalid-email-format' };

      response = await supertest(app)
        .post(ROUTE)
        .send(input);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(400);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given the password does not meet strength requirements', () => {
    beforeAll(async () => {
      const input = { ...mockedVals.validInput, password: 'weakpass', confirmPassword: 'weakpass' };

      response = await supertest(app)
        .post(ROUTE)
        .send(input);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(400);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given signing up with duplicate email or username', () => {
    // set response by running route
    beforeEach(async () => {
      let error = new Error('Error: User validation failed: email: Record with this email already exists.');

      error = {
        "errors": {
          "email": {
            "name": "ValidatorError",
            "message": "Record with this email already exists.",
            "properties": {
              "message": "Record with this email already exists.",
              "type": "unique",
              "path": "email",
              "value":
                "user@mail.local"
            },
            "kind": "unique",
            "path": "email",
            "value": "user@mail.local"
          }
        },
        "_message": "User validation failed",
        "name": "ValidationError",
        "message": "User validation failed: email: Record with this email already exists."
      };
      dbService.createRecord.mockImplementation(() => { throw error; });

      response = await supertest(app)
        .post(ROUTE)
        .send(mockedVals.validInput);
    });
    it('should return a proper status code', () => {
      expect(response.status).toBe(400);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

});