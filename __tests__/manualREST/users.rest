######################################################
# This is a sample collection for the Pocketbase API
# it is build to serve the VSCode rest client extension 
# https://marketplace.visualstudio.com/items?itemName=humao.rest-client
# 
# The following links provide information about the PocketBase API
# https://pocketbase.io/docs
######################################################

#################
# SET VARS
#################
# in VSCode press ctrl+alt+e to switch between admin and user
# vars are stored in /.vscode/settings.json
@token = {{login.response.body.accessToken}}
@token = {{refreshJWT.response.body.accessToken}}

@userId = {{login.response.body.document.id}}

#################
# HANDLE SIGNUP
#################
### signup
# @name signup
POST {{host}}/users
Accept: application/json
Content-Type: application/json

{
  "name": "{{name}}",
  "username": "{{username}}",
  "password": "{{password}}",
  "confirmPassword": "{{password}}",
  "email": "{{email}}"
}

### request verification
# @name requestVerification
POST {{host}}/auth/verification
Accept: application/json
Content-Type: application/json

{
  "email": "{{email}}"
}

### confirm verification
# @name confirmVerification
PATCH {{host}}/auth/verification
Accept: application/json
Content-Type: application/json

{
  "email": "{{email}}",
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY2YTNkYTViYTEwNjUzMmNhZTEyYTYwOSIsImVtYWlsIjoiZW1icnVjaEB6YmgudW5pLWhhbWJ1cmcuZGUiLCJpYXQiOjE3MjIwODUyMTAsImV4cCI6MTcyMjA4ODgxMH0.YaNozo8sdCHcdWn5qDqgZdMjtGPJFqazSVZCZOsXAMc"
}


#################
# HANDLE LOGIN
#################
### login
# @name login
POST {{host}}/auth/login
Accept: application/json
Content-Type: application/json

{
  "password": "{{password}}",
  "email": "{{email}}"
}

### refresh jwt
# @name refreshJWT
GET {{host}}/auth
Authorization: Bearer {{token}}
Accept: application/json


### logout
# @name logout
delete {{host}}/auth

#################
# HANDLE PROFILE
#################
### get one
# @name getOne
GET {{host}}/users/{{userId}}
Authorization: Bearer {{token}}
Accept: application/json

### get multiple
# @name getMultiple
GET {{host}}/users
Authorization: Bearer {{token}}
Accept: application/json

### UPDATE
PATCH {{host}}/users/{{userId}}
content-type: application/json
Authorization: Bearer {{token}}

{
  "name": "{{name}}",
  "username": "{{username}}",
  "email": "{{email}}"
}


### DELETE
DELETE {{host}}/users/66b77c29108488f6000e73cb
content-type: application/json
Authorization: Bearer {{token}}

#################
# HANDLE CHANGES
#################
### request password reset
# @name requestPasswordReset
POST {{host}}/auth/password-reset
Accept: application/json
Content-Type: application/json

{
  "email": "{{email}}"
}

### reset password
# @name resetPassword
PATCH {{host}}/auth/password-reset
Accept: application/json
Content-Type: application/json

{
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY2YTIzMGJhNzMxOTdmODhlNTRiMGU2YSIsImVtYWlsIjoiZW1icnVjaEB6YmgudW5pLWhhbWJ1cmcuZGUiLCJpYXQiOjE3MjE5MDU0NTcsImV4cCI6MTcyMTkwOTA1N30.jIWXpPGttDAbamde7SGHN15cU8bS5gjCACnwZYtQ-gA",
  "password": "{{password}}",
  "confirmPassword": "{{password}}"
}