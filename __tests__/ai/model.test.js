// import vitest, supertest & app
import { vi, beforeAll, beforeEach, describe, expect, expectTypeOf, test, it, afterEach } from 'vitest';
import supertest from "supertest";
import app from "../../app.js";
import jwt from 'jsonwebtoken';

// set route
const ROUTE = '/ai/model';
// prepare response of each test
let response;

// ############################
//  OBJECTS
// ############################


const mockedVals = vi.hoisted(() => {
  return {
    foundUser: {
      _id: '66a29da2942b3eb',
      username: 'snoopy',
      name: 'My User',
      email: 'user@mail.local',
      verified: true,
      role: 0,
      createdAt: '2024-07 - 25T18: 46: 58.982Z',
      updatedAt: '2024-07 - 25T18: 46: 58.982Z',
      __v: 0,
      fullname: '',
      id: '66a29da2942b3ebcaf047f07'
    },
    foundModel: {
      "details": {
        "families": [
          "llama",
        ],
        "family": "llama",
        "format": "gguf",
        "parameter_size": "8.0B",
        "parent_model": "",
        "quantization_level": "Q4_0",
      },
      "license": "someTexts",
      "modelfile": "someTexts",
      "parameters": "someTexts",
      "template": "someTexts"
    },
    validInput: {
      model: 'llama3'
    }
  };
});

// ############################
//  MOCKS
// ############################
import * as dbService from '../../utils/handleDB.js';
// mock dbService
vi.mock('../../utils/handleDB.js', async (importOriginal) => {
  return {
    ...await importOriginal(),
    dbConnection: vi.fn(() => 'mocked'),
    findOneRecord: vi.fn(() => mockedVals.foundUser),
  };
});

// mock aiService
import * as aiService from '../../utils/handleAI.js';
vi.mock('../../utils/handleAI.js', async (importOriginal) => {
  return {
    ...await importOriginal(),
    aiGetModel: vi.fn(() => mockedVals.foundModel)
  };
});

// ############################
//  TESTS
// ############################


describe('ai model', () => {
  const _jwt = () => {
    return jwt.sign({ id: mockedVals.foundUser.id, role: mockedVals.foundUser.role }, process.env.JWT_SECRET_KEY, { expiresIn: process.env.JWT_TTL });
  };

  describe('given the inputs are valid', () => {
    beforeAll(async () => {
      response = await supertest(app)
        .post(ROUTE)
        .set('Authorization', `Bearer ${_jwt()}`)
        .send(mockedVals.validInput);
    });
    it('should return a proper status code', () => {
      expect(response.status).toBe(200);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot({
        license: expect.any(String),
        modelfile: expect.any(String),
        parameters: expect.any(String),
        template: expect.any(String),
      });
    });
  });



  // ############################

  describe('given no matching model found', () => {
    beforeAll(async () => {
      let error = new Error("model 'xxx' not found");
      error.name = 'ResponseError';
      error.status = 400;
      aiService.aiGetModel.mockImplementation(() => { throw error; });

      response = await supertest(app)
        .post(ROUTE)
        .set('Authorization', `Bearer ${_jwt()}`)
        .send({ model: 'xxx' });
    });
    it('should return a proper status code', () => {
      expect(response.status).toBe(500);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given required fields are missing', () => {
    beforeAll(async () => {
      const { model, ...input } = mockedVals.validInput;

      response = await supertest(app)
        .post(ROUTE)
        .set('Authorization', `Bearer ${_jwt()}`)
        .send(input);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(400);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given no valid jwt sended', () => {
    beforeAll(async () => {
      response = await supertest(app)
        .post(ROUTE)
        .set('Authorization', `Bearer invalid`)
        .send(mockedVals.validInput);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(403);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given no jwt sended', () => {
    beforeAll(async () => {
      response = await supertest(app)
        .post(ROUTE)
        .send(mockedVals.validInput);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(401);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

});