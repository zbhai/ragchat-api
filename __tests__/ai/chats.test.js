// import vitest, supertest & app
import { vi, beforeAll, beforeEach, describe, expect, expectTypeOf, test, it, afterEach } from 'vitest';
import supertest from "supertest";
import app from "../../app.js";
import jwt from 'jsonwebtoken';

// set route
const ROUTE = '/ai/chats';
// prepare response of each test
let response;

// ############################
//  OBJECTS
// ############################

const mockedVals = vi.hoisted(() => {
  return {
    foundUser: {
      _id: '66a29da2942b3eb',
      username: 'snoopy',
      name: 'My User',
      email: 'user@mail.local',
      verified: true,
      role: 0,
      createdAt: '2024-07 - 25T18: 46: 58.982Z',
      updatedAt: '2024-07 - 25T18: 46: 58.982Z',
      __v: 0,
      fullname: '',
      id: '66a29da2942b3ebcaf047f07'
    },
    foundChats: {
      "chats": [
        {
          "chatHistory": [
            {
              "data": {
                "additional_kwargs": {},
                "content": "Under what path could members of the working group can find the exam git directory?",
                "response_metadata": {}
              },
              "type": "human"
            },
            {
              "data": {
                "additional_kwargs": {},
                "content": "Members of the working group can find the exam git directory under the following path:\n\n/home/samba/amd/AMD_Lehre/GCI_Grundlagen_Chemieinformatik",
                "invalid_tool_calls": [],
                "response_metadata": {},
                "tool_calls": []
              },
              "type": "ai"
            },
            {
              "data": {
                "additional_kwargs": {},
                "content": "What else can be found under that path?",
                "response_metadata": {}
              },
              "type": "human"
            },
            {
              "data": {
                "additional_kwargs": {},
                "content": "According to the context, members of the working group can also find past semester results under the same path.",
                "invalid_tool_calls": [],
                "response_metadata": {},
                "tool_calls": []
              },
              "type": "ai"
            }
          ],
          "collectionId": "fkhmqgmmxx7svya",
          "collectionName": "chats",
          "created": "2024-07-15 06:17:31.815Z",
          "id": "vwbprn1sxo7qx9k",
          "title": "\"Finding the Exam: A Path to Discovery\"",
          "updated": "2024-07-15 13:12:50.915Z",
          "user": "jr9mt8yvuri3sbd"
        },
        {
          "chatHistory": [
            {
              "data": {
                "additional_kwargs": {},
                "content": "What's the diameter of the planet earth?",
                "response_metadata": {}
              },
              "type": "human"
            },
            {
              "data": {
                "additional_kwargs": {},
                "content": "The diameter of the Earth is approximately 12,742 kilometers (7,918 miles).",
                "invalid_tool_calls": [],
                "response_metadata": {},
                "tool_calls": []
              },
              "type": "ai"
            },
            {
              "data": {
                "additional_kwargs": {},
                "content": "and how far away is the moon?",
                "response_metadata": {}
              },
              "type": "human"
            },
            {
              "data": {
                "additional_kwargs": {},
                "content": "The average distance from the Earth to the Moon is approximately 384,400 kilometers (238,900 miles).",
                "invalid_tool_calls": [],
                "response_metadata": {},
                "tool_calls": []
              },
              "type": "ai"
            }
          ],
          "collectionId": "fkhmqgmmxx7svya",
          "collectionName": "chats",
          "created": "2024-07-08 11:46:22.267Z",
          "id": "yc9vnbs4aj8iux2",
          "title": "\n\"The Earth's Diameter: A Matter of Scale\"",
          "updated": "2024-07-08 11:47:21.830Z",
          "user": "jr9mt8yvuri3sbd"
        }
      ]
    }
  };
});


// ############################
//  MOCKS
// ############################
import * as dbService from '../../utils/handleDB.js';
// mock dbService
vi.mock('../../utils/handleDB.js', async (importOriginal) => {
  return {
    ...await importOriginal(),
    dbConnection: vi.fn(() => 'mocked'),
    findOneRecord: vi.fn(() => mockedVals.foundUser),
    findRecords: vi.fn(() => mockedVals.foundChats),
  };
});

// ############################
//  TESTS
// ############################
describe('ai get users chats', () => {
  const _jwt = (id, role) => {
    return jwt.sign({ id, role }, process.env.JWT_SECRET_KEY, { expiresIn: process.env.JWT_TTL });
  };

  describe('given the inputs are valid', () => {
    beforeAll(async () => {

      response = await supertest(app)
        .get(ROUTE)
        .set('Authorization', `Bearer ${_jwt(mockedVals.foundUser.id, mockedVals.foundUser.role)}`)
        .send();
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(200);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given no valid jwt sended', () => {
    beforeAll(async () => {
      response = await supertest(app)
        .get(ROUTE)
        .set('Authorization', `Bearer invalid`)
        .send();
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(403);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given no jwt sended', () => {
    beforeAll(async () => {
      response = await supertest(app)
        .get(ROUTE)
        .send();
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(401);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });
});