// import vitest, supertest & app
import { vi, beforeAll, beforeEach, describe, expect, expectTypeOf, test, it, afterEach } from 'vitest';
import supertest from "supertest";
import app from "../../app.js";
import jwt from 'jsonwebtoken';

// set route
const ROUTE = '/ai/models';
// prepare response of each test
let response;

// ############################
//  OBJECTS
// ############################
const mockedVals = vi.hoisted(() => {
  return {
    foundUser: {
      _id: '66a29da2942b3eb',
      username: 'snoopy',
      name: 'My User',
      email: 'user@mail.local',
      verified: true,
      role: 4,
      createdAt: '2024-07 - 25T18: 46: 58.982Z',
      updatedAt: '2024-07 - 25T18: 46: 58.982Z',
      __v: 0,
      fullname: '',
      id: '66a29da2942b3ebcaf047f07'
    },
    foundModels: {
      models: [
        {
          "name": "llama3:latest",
          "model": "llama3:latest",
          "modified_at": "2024-07-25T20:25:26.869024101+02:00",
          "size": 4661224676,
          "digest": "365c0bd3c000a25d28ddbf732fe1c6add414de7275464c4e4d1c3b5fcb5d8ad1",
          "details": {
            "parent_model": "",
            "format": "gguf",
            "family": "llama",
            "families": [
              "llama"
            ],
            "parameter_size": "8.0B",
            "quantization_level": "Q4_0"
          }
        },
        {
          "name": "orca2:13b",
          "model": "orca2:13b",
          "modified_at": "2024-06-15T16:53:37.368220025+02:00",
          "size": 7365868139,
          "digest": "a8dcfac3ac32d06f6241896d56928ac7b1d7a6e7f5dcc6b2aec69f2194a9f091",
          "details": {
            "parent_model": "",
            "format": "gguf",
            "family": "llama",
            "families": null,
            "parameter_size": "13B",
            "quantization_level": "Q4_0"
          }
        },
        {
          "name": "llama2:13b",
          "model": "llama2:13b",
          "modified_at": "2024-06-15T16:39:07.956494263+02:00",
          "size": 7366821294,
          "digest": "d475bf4c50bc4d29f333023e38cd56535039eec11052204e5304c8773cc8416c",
          "details": {
            "parent_model": "",
            "format": "gguf",
            "family": "llama",
            "families": [
              "llama"
            ],
            "parameter_size": "13B",
            "quantization_level": "Q4_0"
          }
        },
        {
          "name": "starling-lm:latest",
          "model": "starling-lm:latest",
          "modified_at": "2024-06-15T16:12:29.439449821+02:00",
          "size": 4108940286,
          "digest": "39153f619be614bf1b8b91cf31afe53ec107d70b6b7bb4118aa52bccc107ca7e",
          "details": {
            "parent_model": "",
            "format": "gguf",
            "family": "llama",
            "families": [
              "llama"
            ],
            "parameter_size": "7B",
            "quantization_level": "Q4_0"
          }
        },
        {
          "name": "llama2:latest",
          "model": "llama2:latest",
          "modified_at": "2024-06-13T11:00:05.975159345+02:00",
          "size": 3826793677,
          "digest": "78e26419b4469263f75331927a00a0284ef6544c1975b826b15abdaef17bb962",
          "details": {
            "parent_model": "",
            "format": "gguf",
            "family": "llama",
            "families": [
              "llama"
            ],
            "parameter_size": "7B",
            "quantization_level": "Q4_0"
          }
        },
        {
          "name": "mistral:latest",
          "model": "mistral:latest",
          "modified_at": "2024-06-13T11:00:05.455160458+02:00",
          "size": 4113301090,
          "digest": "2ae6f6dd7a3dd734790bbbf58b8909a606e0e7e97e94b7604e0aa7ae4490e6d8",
          "details": {
            "parent_model": "",
            "format": "gguf",
            "family": "llama",
            "families": [
              "llama"
            ],
            "parameter_size": "7.2B",
            "quantization_level": "Q4_0"
          }
        },
        {
          "name": "mxbai-embed-large:latest",
          "model": "mxbai-embed-large:latest",
          "modified_at": "2024-05-17T20:38:00.241769083+02:00",
          "size": 669615493,
          "digest": "468836162de7f81e041c43663fedbbba921dcea9b9fefea135685a39b2d83dd8",
          "details": {
            "parent_model": "",
            "format": "gguf",
            "family": "bert",
            "families": [
              "bert"
            ],
            "parameter_size": "334M",
            "quantization_level": "F16"
          }
        }
      ]
    },
    validInput: {
      model: 'llama3'
    }
  };
});

// ############################
//  MOCKS
// ############################
import * as dbService from '../../utils/handleDB.js';
// mock dbService
vi.mock('../../utils/handleDB.js', async (importOriginal) => {
  return {
    ...await importOriginal(),
    dbConnection: vi.fn(() => 'mocked'),
    findOneRecord: vi.fn(() => mockedVals.foundUser),
  };
});

// import AI Service
import * as aiService from '../../utils/handleAI.js';
// mock aiService
vi.mock('../../utils/handleAI.js', async (importOriginal) => {
  return {
    ...await importOriginal(),
    aiInstallModel: vi.fn().mockImplementation(() => { return { status: 'success' }; }),
  };
});

// ############################
//  TESTS
// ############################

describe('ai pull model', () => {
  const _jwt = (id, role) => {
    return jwt.sign({ id, role }, process.env.JWT_SECRET_KEY, { expiresIn: process.env.JWT_TTL });
  };

  describe('given the inputs are valid', async () => {
    beforeAll(async () => {
      response = await supertest(app)
        .put(ROUTE)
        .set('Authorization', `Bearer ${_jwt(mockedVals.foundUser.id, mockedVals.foundUser.role)}`)
        .send(mockedVals.validInput);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(200);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });


  // ############################

  describe('given no matching model found', () => {
    beforeAll(async () => {
      const input = { ...mockedVals.validInput, model: 'unknownModel' };

      let error = new Error("pull model manifest: file does not exist");
      error.name = 'ResponseError';
      error.status = 500;
      aiService.aiInstallModel.mockImplementation(() => { throw error; });

      response = await supertest(app)
        .put(ROUTE)
        .set('Authorization', `Bearer ${_jwt(mockedVals.foundUser.id, mockedVals.foundUser.role)}`)
        .send(input);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(500);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given required fields are missing', () => {
    beforeAll(async () => {
      const { model, ...input } = mockedVals.validInput;

      response = await supertest(app)
        .put(ROUTE)
        .set('Authorization', `Bearer ${_jwt(mockedVals.foundUser.id, mockedVals.foundUser.role)}`)
        .send(input);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(400);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given a user tries to access', () => {
    beforeAll(async () => {

      dbService.findOneRecord.mockImplementationOnce(async () => {
        return { ...mockedVals.foundUser, role: 0 };
      });

      response = await supertest(app)
        .put(ROUTE)
        .set('Authorization', `Bearer ${_jwt(mockedVals.foundUser.id, 0)}`)
        .send(mockedVals.validInput);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(403);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given no valid jwt sended', () => {
    beforeAll(async () => {
      response = await supertest(app)
        .put(ROUTE)
        .set('Authorization', `Bearer invalid`)
        .send(mockedVals.validInput);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(403);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given no jwt sended', () => {
    beforeAll(async () => {
      response = await supertest(app)
        .put(ROUTE)
        .send(mockedVals.validInput);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(401);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });
});