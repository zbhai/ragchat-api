// import vitest, supertest & app
import { vi, beforeAll, beforeEach, describe, expect, expectTypeOf, test, it, afterEach } from 'vitest';
import supertest from "supertest";
import app from "../../app.js";
import jwt from 'jsonwebtoken';

// set route
const ROUTE = '/ai/chat';
// prepare response of each test
let response;

// ############################
//  OBJECTS
// ############################
const mockedVals = vi.hoisted(() => {
  return {
    foundUser: {
      _id: '66a29da2942b3eb',
      username: 'snoopy',
      name: 'My User',
      email: 'user@mail.local',
      verified: true,
      role: 4,
      createdAt: '2024-07 - 25T18: 46: 58.982Z',
      updatedAt: '2024-07 - 25T18: 46: 58.982Z',
      __v: 0,
      fullname: '',
      id: '66a29da2942b3ebcaf047f07'
    },
    foundModels: {
      models: [
        {
          "name": "llama3:latest",
          "model": "llama3:latest",
          "modified_at": "2024-07-25T20:25:26.869024101+02:00",
          "size": 4661224676,
          "digest": "365c0bd3c000a25d28ddbf732fe1c6add414de7275464c4e4d1c3b5fcb5d8ad1",
          "details": {
            "parent_model": "",
            "format": "gguf",
            "family": "llama",
            "families": [
              "llama"
            ],
            "parameter_size": "8.0B",
            "quantization_level": "Q4_0"
          }
        },
        {
          "name": "mxbai-embed-large:latest",
          "model": "mxbai-embed-large:latest",
          "modified_at": "2024-05-17T20:38:00.241769083+02:00",
          "size": 669615493,
          "digest": "468836162de7f81e041c43663fedbbba921dcea9b9fefea135685a39b2d83dd8",
          "details": {
            "parent_model": "",
            "format": "gguf",
            "family": "bert",
            "families": [
              "bert"
            ],
            "parameter_size": "334M",
            "quantization_level": "F16"
          }
        }
      ]
    },
    emptyChatRecord: {
      title: 'When Sarcasm Becomes Sweet',
      id: '66a69046251164018e7b1812',
      createdAt: "2024-07 - 28T18: 39:02.388Z",
      updatedAt: "2024-07 - 28T18: 39:02.388Z",
      createdBy: '66a29da2942b3ebcaf047f07',
      __v: 0
    },
    filledChatRecord: {
      title: 'When Sarcasm Becomes Sweet',
      id: '66a69046251164018e7b1812',
      createdAt: "2024-07 - 28T18: 39:02.388Z",
      updatedAt: "2024-07 - 28T18: 39:02.388Z",
      createdBy: '66a29da2942b3ebcaf047f07',
      __v: 0,
      createdBy: "66a29da2942b3ebcaf047f07",
      chatHistory: [
        {
          type: "human",
          data: {
            content: "When does mocking stops feeling like torture?",
            additional_kwargs: {},
            response_metadata: {}
          }
        },
        {
          type: "ai",
          data: {
            content: "It seems you're asking when the pain of mocking (in the context of software development) subsides. Well, with practice and experience, you'll find that your code becomes more robust, and the process of writing unit tests using mocking libraries becomes more natural. The initial discomfort will give way to a sense of confidence and efficiency as you master the art of mocking.",
            source: "pretrained",
            tool_calls: [],
            invalid_tool_calls: [],
            additional_kwargs: {},
            response_metadata: {}
          }
        }
      ]
    },
    answer: "It seems you're asking when the pain of mocking (in the context of software development) subsides. Well, with practice and experience, you'll find that your code becomes more robust, and the process of writing unit tests using mocking libraries becomes more natural. The initial discomfort will give way to a sense of confidence and efficiency as you master the art of mocking.",
    validInput: {
      model: 'llama3',
      input: 'When does mocking stops feeling like torture?',
      chatId: '66a2af22df510b35b401f5ba',
    },
    summaryContent: "Finding salvation: A path through mocking",
  };
});

// ############################
//  MOCKS
// ############################
import * as dbService from '../../utils/handleDB.js';
// mock dbService
vi.mock('../../utils/handleDB.js', async (importOriginal) => {
  return {
    ...await importOriginal(),
    dbConnection: vi.fn(() => 'mocked'),
    findOneRecord: vi.fn(() => mockedVals.foundUser),
    findRecordByID: vi.fn(() => mockedVals.filledChatRecord),
    createRecord: vi.fn(() => mockedVals.emptyChatRecord),
  };
});
// import AI Service
import * as aiService from '../../utils/handleAI.js';
// mock aiService
vi.mock('../../utils/handleAI.js', async (importOriginal) => {
  return {
    ...await importOriginal(),
    aiGetModels: vi.fn(() => mockedVals.foundModels),
    summarizeText: vi.fn(() => mockedVals.summaryContent),
    chat: vi.fn().mockImplementation((req, res, next) => {
      return res.json({ answer: mockedVals.answer, chat: mockedVals.filledChatRecord });
    })
  };
});

// ############################
//  TESTS
// ############################

describe('ai chat with model', () => {
  const _jwt = (id, role) => {
    return jwt.sign({ id, role }, process.env.JWT_SECRET_KEY, { expiresIn: process.env.JWT_TTL });
  };

  describe('given a chatId was sended', () => {
    beforeAll(async () => {

      // dbService.findOneRecord.mockImplementationOnce(() => mockedVals.filledChatRecord);

      response = await supertest(app)
        .post(ROUTE)
        .set('Authorization', `Bearer ${_jwt(mockedVals.foundUser.id, mockedVals.foundUser.role)}`)
        .send(mockedVals.validInput);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(200);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given invalid chatId sended', () => {
    beforeAll(async () => {

      dbService.findRecordByID.mockImplementationOnce(() => null);

      response = await supertest(app)
        .post(ROUTE)
        .set('Authorization', `Bearer ${_jwt(mockedVals.foundUser.id, mockedVals.foundUser.role)}`)
        .send(mockedVals.validInput);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(404);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });


  // ############################

  describe('given no chatId sended', () => {
    beforeAll(async () => {
      const { chatId, ...input } = mockedVals.validInput;
      response = await supertest(app)
        .post(ROUTE)
        .set('Authorization', `Bearer ${_jwt(mockedVals.foundUser.id, mockedVals.foundUser.role)}`)
        .send(input);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(200);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });


  // ############################

  describe('given no matching model found', () => {
    beforeAll(async () => {
      const input = { ...mockedVals.validInput, model: 'unknownModel' };

      aiService.aiGetModels.mockImplementation(() => { return { models: [] }; });


      response = await supertest(app)
        .post(ROUTE)
        .set('Authorization', `Bearer ${_jwt(mockedVals.foundUser.id, mockedVals.foundUser.role)}`)
        .send(input);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(500);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given required fields are missing', () => {
    beforeAll(async () => {
      const { model, ...input } = mockedVals.validInput;

      response = await supertest(app)
        .post(ROUTE)
        .set('Authorization', `Bearer ${_jwt(mockedVals.foundUser.id, mockedVals.foundUser.role)}`)
        .send(input);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(400);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given no valid jwt sended', () => {
    beforeAll(async () => {
      response = await supertest(app)
        .post(ROUTE)
        .set('Authorization', `Bearer invalid`)
        .send(mockedVals.validInput);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(403);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given no jwt sended', () => {
    beforeAll(async () => {
      response = await supertest(app)
        .post(ROUTE)
        .send(mockedVals.validInput);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(401);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

});