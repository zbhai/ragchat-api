// import vitest, supertest & app
import { vi, beforeAll, beforeEach, describe, expect, expectTypeOf, test, it, afterEach } from 'vitest';
import supertest from "supertest";
import app from "../../app.js";
import jwt from 'jsonwebtoken';

// set route
const ROUTE = '/ai/models';
// prepare response of each test
let response;

// ############################
//  OBJECTS
// ############################
const mockedVals = vi.hoisted(() => {
  return {
    foundUser: {
      _id: '66a29da2942b3eb',
      username: 'snoopy',
      name: 'My User',
      email: 'user@mail.local',
      verified: true,
      role: 4,
      createdAt: '2024-07 - 25T18: 46: 58.982Z',
      updatedAt: '2024-07 - 25T18: 46: 58.982Z',
      __v: 0,
      fullname: '',
      id: '66a29da2942b3ebcaf047f07'
    },
    validInput: {
      model: 'llama3'
    }
  };
});

// ############################
//  MOCKS
// ############################
import * as dbService from '../../utils/handleDB.js';
// mock dbService
vi.mock('../../utils/handleDB.js', async (importOriginal) => {
  return {
    ...await importOriginal(),
    dbConnection: vi.fn(() => 'mocked'),
    findOneRecord: vi.fn(() => mockedVals.foundUser),
  };
});

// import AI Service
import * as aiService from '../../utils/handleAI.js';
// mock aiService
vi.mock('../../utils/handleAI.js', async (importOriginal) => {
  return {
    ...await importOriginal(),
    aiDeleteModel: vi.fn().mockImplementation(() => { return { status: 'success' }; }),
  };
});

// ############################
//  TESTS
// ############################

describe('ai delete model', () => {
  const _jwt = (id, role) => {
    return jwt.sign({ id, role }, process.env.JWT_SECRET_KEY, { expiresIn: process.env.JWT_TTL });
  };

  describe('given the inputs are valid', async () => {
    beforeAll(async () => {
      response = await supertest(app)
        .delete(ROUTE)
        .set('Authorization', `Bearer ${_jwt(mockedVals.foundUser.id, mockedVals.foundUser.role)}`)
        .send(mockedVals.validInput);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(200);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given no matching model found', () => {
    beforeAll(async () => {
      const input = { ...mockedVals.validInput, model: 'unknownModel' };

      let error = new Error("pull model manifest: file does not exist");
      error.name = 'ResponseError';
      error.status = 500;
      aiService.aiDeleteModel.mockImplementation(() => { throw error; });

      response = await supertest(app)
        .delete(ROUTE)
        .set('Authorization', `Bearer ${_jwt(mockedVals.foundUser.id, mockedVals.foundUser.role)}`)
        .send(input);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(500);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });


  // ############################

  describe('given required fields are missing', () => {
    beforeAll(async () => {
      const { model, ...input } = mockedVals.validInput;

      response = await supertest(app)
        .delete(ROUTE)
        .set('Authorization', `Bearer ${_jwt(mockedVals.foundUser.id, mockedVals.foundUser.role)}`)
        .send(input);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(400);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given a user tries to access', () => {
    beforeAll(async () => {

      dbService.findOneRecord.mockImplementationOnce(async () => {
        return { ...mockedVals.foundUser, role: 0 };
      });

      response = await supertest(app)
        .delete(ROUTE)
        .set('Authorization', `Bearer ${_jwt(mockedVals.foundUser.id, 0)}`)
        .send(mockedVals.validInput);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(403);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given no valid jwt sended', () => {
    beforeAll(async () => {
      response = await supertest(app)
        .delete(ROUTE)
        .set('Authorization', `Bearer invalid`)
        .send(mockedVals.validInput);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(403);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given no jwt sended', () => {
    beforeAll(async () => {
      response = await supertest(app)
        .delete(ROUTE)
        .send(mockedVals.validInput);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(401);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

});