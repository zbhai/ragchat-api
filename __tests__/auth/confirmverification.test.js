
// import vitest, supertest & app
import { vi, beforeAll, beforeEach, describe, expect, expectTypeOf, test, it, afterEach } from 'vitest';
import supertest from "supertest";
import app from "../../app.js";

// set route
const ROUTE = '/auth/verification';
// prepare response of each test
let response;

// ############################
//  OBJECTS
// ############################
const mockedVals = vi.hoisted(() => {
  return {
    foundUser: {
      _id: '66a29da2942b3eb',
      username: 'snoopy',
      name: 'My User',
      email: 'user@mail.local',
      verified: true,
      role: 0,
      createdAt: '2024-07 - 25T18: 46: 58.982Z',
      updatedAt: '2024-07 - 25T18: 46: 58.982Z',
      __v: 0,
      id: '66a29da2942b3ebcaf047f07'
    },
    validInput: {
      token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbGIBBERISHTl9.lxQ5ZqO8qWJt15bbnSa4wrPQ02_7fvY4CgN1ZRM'
    },
    jwtPayload: {
      "id": "66a29da2942b3ebcaf047f07",
      "email": "user@mail.local",
      "iat": 1722018249,
      "exp": 1722021849
    },
    jwtError: {
      "name": "TokenExpiredError",
      "message": "jwt expired",
      "expiredAt": "2024-07-26T18:18:19.000Z"
    }
  };
});

// ############################
//  MOCKS
// ############################
// import Database Service
import * as dbService from '../../utils/handleDB.js';
// mock dbService
vi.mock('../../utils/handleDB.js', async (importOriginal) => {
  return {
    ...await importOriginal(),
    dbConnection: vi.fn(() => 'mocked'),
    findOneRecord: vi.fn(() => mockedVals.foundUser),
    findOneAndUpdate: vi.fn(() => mockedVals.foundUser)
  };
});
// import Token Service
import * as tokenService from '../../utils/handleTokens.js';
// mock dbService
vi.mock('../../utils/handleTokens.js', async (importOriginal) => {
  return {
    ...await importOriginal(),
    verifyVerificationToken: vi.fn((req, res, next) => next()),
  };
});


// ############################
//  TESTS
// ############################
describe('user verify registration token', () => {


  describe('given the inputs are valid', async () => {
    // set response by running route
    beforeAll(async () => {
      response = await supertest(app)
        .patch(ROUTE)
        .send(mockedVals.validInput);
    });
    it('should return a proper status code', () => {
      expect(response.status).toBe(200);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });


  // ############################

  describe('given the token is invalid', async () => {
    // set response by running route
    beforeAll(async () => {
      tokenService.verifyVerificationToken.mockImplementation((req, res, next) => {
        return res.status(498).json({ message: 'Token is no longer valid.' });
      });

      const input = { ...mockedVals.validInput, token: 'invalid-token' };

      response = await supertest(app)
        .patch(ROUTE)
        .send(input);
    });

    it('should return a proper status code', () => {
      expect(response.status).toBe(498);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given the request body is empty', async () => {
    // set response by running route
    beforeAll(async () => {
      response = await supertest(app)
        .patch(ROUTE)
        .send();
    });
    it('should return a proper status code', () => {
      expect(response.status).toBe(400);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });
});