// import vitest, supertest & app
import { vi, beforeAll, beforeEach, describe, expect, expectTypeOf, test, it, afterEach } from 'vitest';
import supertest from "supertest";
import app from "../../app.js";
// set route
const ROUTE = '/auth';
// prepare response of each test
let response;

// ############################
//  OBJECTS
// ############################
const mockedVals = vi.hoisted(() => {
  return {
    foundUser: {
      _id: '66a29da2942b3eb',
      username: 'snoopy',
      name: 'My User',
      email: 'user@mail.local',
      verified: true,
      role: 0,
      createdAt: '2024-07 - 25T18: 46: 58.982Z',
      updatedAt: '2024-07 - 25T18: 46: 58.982Z',
      __v: 0,
      password: 'StrongPass1!',
      // password,
      id: '66a29da2942b3ebcaf047f07'
    }
  };
});
// ############################
//  MOCKS
// ############################
// import Database Service
import * as dbService from '../../utils/handleDB.js';
// mock dbService
vi.mock('../../utils/handleDB.js', async (importOriginal) => {
  return {
    ...await importOriginal(),
    dbConnection: vi.fn(() => 'mocked'),
    findOneRecord: vi.fn(() => mockedVals.foundUser),
    findOneAndUpdate: vi.fn(() => mockedVals.foundUser),
  };
});

// ############################
//  TESTS
// ############################
describe('user logout', () => {
  // ############################
  describe('given refresh token is valid', () => {
    beforeAll(async () => {
      response = await supertest(app)
        .get(ROUTE)
        .set('Cookie', 'refreshToken=valid');
    });
    it('should return a proper status code', () => {
      expect(response.status).toBe(400);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given refresh token is invalid', () => {
    beforeAll(async () => {
      response = await supertest(app)
        .get(ROUTE)
        .set('Cookie', 'refreshToken=invalid');
    });
    it('should return a proper status code', () => {
      expect(response.status).toBe(400);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given no cookie was send', () => {
    beforeAll(async () => {
      response = await supertest(app)
        .delete(ROUTE);
    });
    it('should return a proper status code', () => {
      expect(response.status).toBe(200);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });
});