// import vitest, supertest & app
import { vi, beforeAll, beforeEach, describe, expect, expectTypeOf, test, it, afterEach } from 'vitest';
import supertest from "supertest";
import app from "../../app.js";
// set route
const ROUTE = '/auth/password-reset';
// prepare response of each test
let response;

// ############################
//  OBJECTS
// ############################
const mockedVals = vi.hoisted(() => {
  return {
    foundUser: {
      _id: '66a29da2942b3eb',
      username: 'snoopy',
      name: 'My User',
      email: 'user@mail.local',
      verified: true,
      role: 0,
      createdAt: '2024-07 - 25T18: 46: 58.982Z',
      updatedAt: '2024-07 - 25T18: 46: 58.982Z',
      __v: 0,
      password: 'StrongPass1!',
      id: '66a29da2942b3ebcaf047f07'
    },
    validInput: {
      email: 'user@mail.local'
    }
  };
});

// ############################
//  MOCKS
// ############################
// import Database Service
import * as dbService from '../../utils/handleDB.js';
// mock dbService
vi.mock('../../utils/handleDB.js', async (importOriginal) => {
  return {
    ...await importOriginal(),
    dbConnection: vi.fn(() => 'mocked'),
    findOneRecord: vi.fn(() => mockedVals.foundUser),
    updateOneRecord: vi.fn(() => { return { ...mockedVals.foundUser, resetPasswordToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY2MOCKED' }; })
  };
});
// mock mailer
vi.mock('../../utils/handleMailer.js', async (importOriginal) => {
  return {
    ...await importOriginal(),
    sendEmail: vi.fn(() => 'mocked')
  };
});

// ############################
//  TESTS
// ############################
describe('user request password reset', () => {
  describe('given the inputs are valid', async () => {
    // set response by running route
    beforeAll(async () => {
      response = await supertest(app)
        .post(ROUTE)
        .send(mockedVals.validInput);
    });
    it('should return a proper status code', () => {
      expect(response.status).toBe(200);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });


  // ############################

  describe('given the email is unknown', async () => {
    // set response by running route
    beforeAll(async () => {
      dbService.findOneRecord.mockImplementationOnce(() => null);

      response = await supertest(app)
        .post(ROUTE)
        .send(mockedVals.validInput);
    });
    it('should return a proper status code', () => {
      expect(response.status).toBe(200);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given the email format is invalid', () => {
    beforeAll(async () => {
      const input = { ...mockedVals.validInput, email: 'invalid-email-format' };

      response = await supertest(app)
        .post(ROUTE)
        .send(input);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(400);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('the request body is empty', () => {
    beforeAll(async () => {
      response = await supertest(app)
        .post(ROUTE)
        .send();
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(400);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });
});