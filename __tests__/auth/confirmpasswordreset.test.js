// import vitest, supertest & app
import { vi, beforeAll, beforeEach, describe, expect, expectTypeOf, test, it, afterEach } from 'vitest';
import supertest from "supertest";
import app from "../../app.js";
import jwt from 'jsonwebtoken';

// set route
const ROUTE = '/auth/password-reset';
// prepare response of each test
let response;

// ############################
//  OBJECTS
// ############################
const mockedVals = vi.hoisted(() => {
  return {
    foundUser: {
      _id: '66a29da2942b3eb',
      username: 'snoopy',
      name: 'My User',
      email: 'user@mail.local',
      verified: true,
      role: 0,
      createdAt: '2024-07 - 25T18: 46: 58.982Z',
      updatedAt: '2024-07 - 25T18: 46: 58.982Z',
      __v: 0,
      password: 'StrongPass1!',
      resetPasswordToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY2MOCKED',
      id: '66a29da2942b3ebcaf047f07'
    },
    validInput: {
      token: 'invalidToken',
      password: 'SuperPW123!',
      confirmPassword: 'SuperPW123!'
    }
  };
});

// ############################
//  MOCKS
// ############################
// import Database Service
import * as dbService from '../../utils/handleDB.js';
// mock dbService
vi.mock('../../utils/handleDB.js', async (importOriginal) => {
  return {
    ...await importOriginal(),
    dbConnection: vi.fn(() => 'mocked'),
    findOneRecord: vi.fn(() => mockedVals.foundUser),
    updateOneRecord: vi.fn(() => mockedVals.foundUser)
  };
});

// import Token Service
import * as tokenService from '../../utils/handleTokens.js';
// mock dbService
vi.mock('../../utils/handleTokens.js', async (importOriginal) => {
  return {
    ...await importOriginal(),
    verifyVerificationToken: vi.fn((req, res, next) => next()),
  };
});

// ############################
//  TESTS
// ############################
describe('user confirm password reset', () => {

  describe('given the inputs are correct', async () => {
    const token = jwt.sign({ id: mockedVals.foundUser.id, email: mockedVals.foundUser.email }, process.env.PASSWORD_TOKEN_KEY + mockedVals.foundUser.password, { expiresIn: process.env.PASSWORD_TOKEN_TTL });

    // set response by running route
    beforeAll(async () => {

      const input = { ...mockedVals.validInput, token };

      response = await supertest(app)
        .patch(ROUTE)
        .send(input);
    });

    it('should return a proper status code', () => {
      expect(response.status).toBe(200);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });


  // ############################
  describe('given refresh token is malformed', () => {
    const input = { ...mockedVals.validInput, token: 'malformed-Token' };

    beforeAll(async () => {
      response = await supertest(app)
        .patch(ROUTE)
        .send(input);
    });
    it('should return a proper status code', () => {
      expect(response.status).toBe(498);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given token is invalid', () => {
    beforeAll(async () => {

      const input = { ...mockedVals.validInput, token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY2YTNkYTViYTEwNjUzMmNhZTEyYTYwOSIsImlhdCI6MTcyMjA5ODM3OX0.7Pq8F2zSDwuEzlCQX3vMZAw9D43N6dSViCyVPZ_s_Zs' };

      response = await supertest(app)
        .patch(ROUTE)
        .send(input);
    });
    it('should return a proper status code', () => {
      expect(response.status).toBe(498);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given required fields are missing', () => {
    beforeAll(async () => {
      const { confirmPassword, ...input } = mockedVals.validInput;

      response = await supertest(app)
        .patch(ROUTE)
        .send(input);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(400);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given the password and confirmPassword do not match', () => {
    beforeAll(async () => {
      const input = { ...mockedVals.validInput, confirmPassword: 'StrongPass2!' };

      response = await supertest(app)
        .patch(ROUTE)
        .send(input);
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(400);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('the request body is empty', () => {
    beforeAll(async () => {
      response = await supertest(app)
        .patch(ROUTE)
        .send();
    });

    it('should return a proper status code status', () => {
      expect(response.status).toBe(400);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

});