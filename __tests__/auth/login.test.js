// import vitest, supertest & app
import { vi, beforeAll, beforeEach, describe, expect, expectTypeOf, test, it, afterEach } from 'vitest';
import supertest from "supertest";
import app from "../../app.js";
import bcrypt from 'bcrypt';

// set route
const ROUTE = '/auth/login';
// prepare response of each test
let response;

// ############################
//  OBJECTS
// ############################
const mockedVals = vi.hoisted(() => {
  return {
    foundUser: {
      _id: '66a29da2942b3eb',
      username: 'snoopy',
      name: 'My User',
      email: 'user@mail.local',
      verified: true,
      role: 0,
      createdAt: '2024-07 - 25T18: 46: 58.982Z',
      updatedAt: '2024-07 - 25T18: 46: 58.982Z',
      __v: 0,
      password: 'StrongPass1!',
      // password,
      id: '66a29da2942b3ebcaf047f07'
    },
    validInput: {
      email: 'user@mail.local',
      password: 'StrongPass1!'
    }
  };
});

// ############################
//  MOCKS
// ############################
// import Database Service
import * as dbService from '../../utils/handleDB.js';
// mock dbService
vi.mock('../../utils/handleDB.js', async (importOriginal) => {
  return {
    ...await importOriginal(),
    dbConnection: vi.fn(() => 'mocked'),
    findOneRecord: vi.fn(() => mockedVals.foundUser),
    findByIdAndUpdate: vi.fn(() => { return { ...mockedVals.foundUser, refreshToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY2MOCKED' }; })
  };
});

// ############################
//  TESTS
// ############################
describe('user login', async () => {
  // prepare a hash function with current .env bcrypt settings
  const _hashPw = async () => {
    return await bcrypt.hash(mockedVals.foundUser.password, Number(process.env.BCRYPT_STRENGTH));
  };

  describe('given the inputs are valid', () => {
    beforeAll(async () => {
      //  hash password
      dbService.findOneRecord.mockImplementationOnce(async () => {
        return { ...mockedVals.foundUser, password: await _hashPw() };
      });

      // set response by running route
      response = await supertest(app)
        .post(ROUTE)
        .send(mockedVals.validInput);
    });

    it('should return a proper status code', () => {
      expect(response.status).toBe(200);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot({
        accessToken: expect.any(String),
      });
    });
  });

  // ############################

  describe('given email and password are valid, but accout is unverified', () => {

    beforeAll(async () => {
      //  hash password & unverify user
      dbService.findOneRecord.mockImplementationOnce(async () => {
        return { ...mockedVals.foundUser, verified: false, password: await _hashPw() };
      });

      // set response by running route
      response = await supertest(app)
        .post(ROUTE)
        .send(mockedVals.validInput);
    });

    it('should return a proper status code', () => {
      expect(response.status).toBe(401);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given the password is wrong', async () => {
    // set response by running route
    beforeAll(async () => {

      //  hash password
      dbService.findOneRecord.mockImplementationOnce(async () => {
        return { ...mockedVals.foundUser, password: await _hashPw() };
      });

      const input = { ...mockedVals.validInput, password: 'invalid-password' };

      response = await supertest(app)
        .post(ROUTE)
        .send(input);
    });
    it('should return a proper status code', () => {
      expect(response.status).toBe(401);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given the email is unknown', async () => {
    // set response by running route
    beforeAll(async () => {
      dbService.findOneRecord.mockImplementationOnce(() => null);

      response = await supertest(app)
        .post(ROUTE)
        .send(mockedVals.validInput);
    });
    it('should return a proper status code', () => {
      expect(response.status).toBe(401);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });

  // ############################

  describe('given the request body is empty', async () => {
    // set response by running route
    beforeAll(async () => {
      response = await supertest(app)
        .post(ROUTE)
        .send();
    });
    it('should return a proper status code', () => {
      expect(response.status).toBe(400);
    });
    it('should respond with a proper body', () => {
      expect(response.body).toMatchSnapshot();
    });
  });
});