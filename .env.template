##########
# BACKEND SERVER
##########
# THIS FORCES NODE TO IGNORE ERRORS ON SELF-SIGNED CERTIFICATES
# especially useful while testing with vitest
NODE_TLS_REJECT_UNAUTHORIZED = 0

# define if api is live or in development {devel|prod}
API_MODE=devel

# port the api shall listen on
API_PORT=8080

# hostname/IP-address the api shall listen on
API_HOST=localhost

# path to SSL-Key-File
API_SSL_KEY=./utils/ssl/key_localhost.pem

# path to SSL-Cert-File
API_SSL_CERT=./utils/ssl/cert_localhost.pem

# URL to mongodb incl. username & password
MONGO_URL=mongodb://mongo.company.local:27017/ragchat

# admin email | will be created on app start and can be used to login afterwards
SUPERADMIN_EMAIL=admin@company.local

# admin password - The length must be between 10 and 72 | will be created on first start and used to login afterwards
# won't changed if SUPERADMIN_EMAIL is already in use
SUPERADMIN_PASSWORD=SuperSecret123!

# if true timing measurements will be logged
DEBUG_PERFORMANCE=false

##########
# SECURITY
##########
### passwords
# set hash strength for user passwords (higher is more secure but slower)
BCRYPT_STRENGTH=10

# JWT
### access-JWT
# Secret to hash access-JWT for accessing non-public pages
JWT_SECRET_KEY=1b54b404da66GIBBERISHd5903260c3de17df236465cbe40dbaec0f57eefe2668c0fd00ba589b093dd1a36c77c57a1f20d6165b963badd7e68d8f12e
# how long is the access-JWT valid (XXs|m|h)
JWT_TTL=10m

### refresh-JWT
# Secret to hash refresh-JWT for requesting a new access-JWT
JWT_REFRESH_KEY=a2d4a05792e62b33c54e8e94febbbd7ad370a9554cd909cc9197be35bac7d68ff47b6bceb75cedf3aGIBBERISH8859f7649305ff60633e99f96e244c52fc

### password-JWT
# Secret to hash password-JWT for validating the account
VERIFICATION_TOKEN_KEY=6C776BGIBBERISHDF36A7F7D467C0E4F1C44491D9BA450C8911C4B3D9AFE1E0E50C94A82D71593B4305337D46FE86E585F93F4546E2EA99
# how long is the validation-JWT valid (XXs|m|h)
VERIFICATION_TOKEN_TTL=1h

### password-JWT
# Secret to hash password-JWT for requesting a password reset
PASSWORD_TOKEN_KEY=6C166BA60B27A3087EB3B9B72434GIBBERISH0E4F1C44491D9BA450C8911C4B3D9AFE1E0E50C94A82D71593B4305337D46FE86E585F93F4546E2EAC8
# how long is the password-JWT valid (XXs|m|h)
PASSWORD_TOKEN_TTL=1h

##########
# FRONTEND
##########
# valid URL, the frontend is running on
# will be used by cors to allow access to API
# will be used in eMail templates (i.e. password reset)
FRONTEND_URL=http://localhost,http://127.0.0.1

##########
# MAIL
##########
# hostname of the mail server
SMTP_HOST=mail.company.local

# mail server port
SMTP_PORT=587

# secured by ssl/tls [true|false]?
SMTP_SECURE=true

# username to log in at mail server
SMTP_USER=mailuser

# password to log in at mail server
SMTP_PASSWORD='SuperSecret123!'

# address the mail should come from
SMTP_FROM_ADDRESS=ragchat@company.local

# display name the mail should come from
SMTP_FROM_NAME=RAGChat

##########
# OLLAMA API
##########
# the hostname/IP-address of the OLLAMA API
AI_API_URL=http://ollama.host.local:11434

# the prompt to set the LLM in the mood
AI_CHAT_PROMPT="You are a Professor. Restrict the answer to the topic. State the answer as clear and precise as possible. Avoid to be chatty. Don't respond with technical details. If the answer is retrieved from the documents, begin your response with '<DS>' and provide the relevant details or excerpts from the document. If the answer is generated from your pretrained knowledge, begin your response with '<PK>:' and then provide the answer. Never return an answer without those tags. \n
\n
{context}"

# the prompt to let the LLM summarize the former conversation
AI_CONTEXTUALIZE_PROMPT="Given the above conversation, generate a search query to look up in order to get information relevant to the conversation."

#the prompt to summarize the first user of a chat; Uses as title of the chat
AI_SUMMARIZE_PROMPT="Summarize the following user input. Make it as short as possible. Let it sound like a book title.\n
\n
{text}"

# the temperature to allow the LLM to be creative. accepts a value between 0 and 1.
# 0 means the LLM will be very conservative, 1 means the LLM will be very creative.
AI_TEMPERATURE=0

##########
# POCKETBASE
##########
# the hostname/IP-address:Port of the Pocketbase API
# while project is running, PB is also accessible via Browser at <PB_API_URL>/_/#/login
PB_API_URL=http://127.0.0.1:8090

# admin email | will be created on first start and used to login afterwards
# ! never change this after first start !
PB_ADMIN_EMAIL=admin@company.local

# admin password - The length must be between 10 and 72 | will be created on first start and used to login afterwards
# ! never change this after first start !
PB_ADMIN_PASSWORD=SuperSecret123!

# JWT secret to sign the JWT - The length must be between 30 and 300.
PB_JWT_SECRET=UJXLUSDZgibberishQ5LSx0xQ09XFR

# how many seconds is the JWT valid
PB_JWT_EXPIRES_IN=3600

##########
# RAG
##########
# url to the vector database
VECTOR_API_URL=http://vector.host.local:11435

# name of the collection to store the vectors
VECTOR_COLLECTION_NAME=onboarding

# embedding model name
RAG_MODEL_NAME=mxbai-embed-large

# folder to fetch the RAG Files from [relative to the project root]
RAG_FOLDER=/RAGFiles

# delete embeddings if RAG File is deleted [true|false]
RAG_DELETE_EMBEDDINGS=true

# [optional] enable tracing to LangSmith https://docs.smith.langchain.com/
LANGCHAIN_TRACING_V2=true
# if LANGCHAIN_TRACING_V2 === true API-Key is needed
LANGCHAIN_API_KEY=lsv2_sk_c580367cGibberish82