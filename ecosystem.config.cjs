module.exports = {
  apps: [{
    name: "RAGchat-API",
    script: "server.js",
    cwd: "/local/embruch/ragchat-api",
    node_args: "--env-file=/local/embruch/ragchat-api/.env",
    watch: true,
    instances: "8",
    exec_mode: "cluster",
    ignore_watch: ["./node_modules", "./RAGFiles", "./.git", "./coverage", "./logs", "./__tests__"],
    error_file: 'logs/error.log',
    out_file: 'logs/output.log'
  }]
};